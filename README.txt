DESCRIPTION:
------------
This module adds the permissions to edit their own and all user profiles.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'modules' or directory.
2. Place 'Access profile edit' in the modules directory for your site and
   enable it.
3. The module to add two permissions: 'User can edit their own profile' and
   'User can edit all profiles'.

CONFIGURATION
-------------
1. Visit /admin/people/permissions page.
2. Check 'User can edit their own profile' or 'User can edit all profiles'
   permission.
3. 'User can edit all profiles' depends on 'View user profiles' permission.
